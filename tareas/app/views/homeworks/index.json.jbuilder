json.array!(@homeworks) do |homework|
  json.extract! homework, :id, :materia, :tiempo, :nombre, :observaciones
  json.url homework_url(homework, format: :json)
end
