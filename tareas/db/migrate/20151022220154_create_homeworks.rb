class CreateHomeworks < ActiveRecord::Migration
  def change
    create_table :homeworks do |t|
      t.string :materia
      t.integer :tiempo
      t.string :nombre
      t.string :observaciones

      t.timestamps null: false
    end
  end
end
